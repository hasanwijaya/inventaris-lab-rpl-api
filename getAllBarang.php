<?php

    if($_SERVER['REQUEST_METHOD']=='GET'){

        include "config.php";

        $sql = "SELECT * FROM barang ORDER BY b_kode";
        $query = mysqli_query($db, $sql);
            
        if($query){
            while ($barang = mysqli_fetch_assoc($query)) {
                $array_data[] = $barang;
            }
            $response["status"] = true;
            $response["message"] = "Sukses get data";
            $response["data"] = $array_data;
            echo json_encode($response); 
        }else{
            $response["status"] = false;
            $response["message"] = "Gagal get data";
            $response["data"] = null;
            echo json_encode($response); 
        }
        
        mysqli_close($db);
    }

?>